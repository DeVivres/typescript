import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
export function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
const fightersDetailsCache = new Map();
async function showFighterDetails(event, fighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
}
export async function getFighterInfo(fighterId) {
    const fighterInfo = await getFighterDetails(fighterId);
    return fighterInfo;
}
function createFightersSelector() {
    const selectedFighters = new Map();
    return async function selectFighterForBattle(event, fighter) {
        const fullInfo = await getFighterInfo(fighter._id);
        const eventTarget = event.target;
        if (eventTarget.checked) {
            selectedFighters.set(fighter._id, fullInfo);
        }
        else {
            selectedFighters.delete(fighter._id);
        }
        if (selectedFighters.size === 2) {
            const fighters = Array.from(selectedFighters.values());
            const winner = fight(fighters[0], fighters[1]);
            showWinnerModal(winner);
        }
    };
}
