var path = require('path');

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [{
            test: /\.(ts|js)x?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              configFile: "./babel.config.js",
              cacheDirectory: true
            }
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        }
      ],
    },
    mode: 'development',
    devServer: {
      inline: true
    },
    devtool: "source-map"
}