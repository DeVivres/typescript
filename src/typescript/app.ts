import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { Fighter } from './fighterModel';


const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

export async function startApp(): Promise<void> {
  if (rootElement != null && loadingElement != null) {
    try {
      loadingElement.style.visibility = 'visible';
      
      const fighters = await getFighters();
      const fightersElement = createFighters(fighters as Fighter[]);

      rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    } finally {
      loadingElement.style.visibility = 'hidden';
    }
  }
}
